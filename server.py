# first of all import the socket library
import socket  

def server(): 
    host = '127.0.0.1'
    port = 12345              
# next create a socket object
    s = socket.socket()        
    s.bind((host, port))            
 
# put the socket into listening mode
    s.listen(2)
# Establish connection with client. 
    c, addr = s.accept()            
    print("Got connection from " + str(addr)) 

#loop number
    num = 1
    n = 1
    while num <= 100:
      data = c.recv(1024).decode()
      print("From connected Number " + str(data)) 
      num = int(data) + n
      c.send(str(num).encode())
    
# Close the connection with the client
    print("Finish Connected")
    c.close()

if __name__ == '__main__':
    server()
   