# Import socket module
import socket  

def client(): 
        host = '127.0.0.1'  
        port = 12345  
# Create a socket object
        cli = socket.socket()  
# connect to the server on local computer
        cli.connect((host, port))

        num = 1
        while num <= 100:
           cli.send(str(num).encode())
           data = cli.recv(1024).decode() # receive data from the server and decoding to get the string.
           print('Received from server: ' + data)
           num = int(data) + 1
           
# close the connection
        print('Finish Connected')
        cli.close()

if __name__ == '__main__':
    client()
